#!/usr/bin/env python3
import rospy
from sensor_msgs.msg import LaserEcho,LaserScan,MultiEchoLaserScan

class laser_converter():
    def __init__(self):
        rospy.Subscriber("/horizontal_laser_2d",MultiEchoLaserScan,self.echo_cb)
        self.pub = rospy.Publisher("/laser_scan",LaserScan,queue_size=10)
        self.laser_data = LaserScan()

        self.ranges = []
        self.intensity = []

    def echo_cb(self,data):
        self.laser_data.header = data.header
        self.laser_data.header.frame_id = "laser_frame"
        self.laser_data.angle_min = data.angle_min
        self.laser_data.angle_max = data.angle_max
        self.laser_data.angle_increment = data.angle_increment
        self.laser_data.time_increment = data.time_increment

        # self.ranges = data.ranges
        self.intensity = data.intensities
        echo = 0

        # self.ranges_list = (data.ranges[0])
        # self.laser_data.ranges = (self.ranges_list)
        # self.laser_data.intensities = self.intensity
        # print(le   n(data.ranges[0].echoes))

        for i in range(len(data.ranges)-1):
            self.ranges.append(float(data.ranges[i].echoes[0]))
            # print(data.ranges[i].echoes)

        self.laser_data.ranges = self.ranges
        # print(type(self.ranges))
        # print(type(self.laser_data.ranges))
        # print(type(data.ranges[2].echoes[0]))
        
        self.pub.publish(self.laser_data)

if __name__ == "__main__":
    rospy.init_node('laser_converter')
    while not rospy.is_shutdown():
        laser_converter()
        rospy.spin()


